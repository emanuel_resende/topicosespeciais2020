// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAspneXz0y9uRB8x7xoYGlUdi8TR7dgDNU',
    authDomain: 'controleifsp.firebaseapp.com',
    databaseURL: 'https://controleifsp.firebaseio.com',
    projectId: 'controleifsp',
    storageBucket: 'controleifsp.appspot.com',
    messagingSenderId: '330643600079',
    appId: '1:330643600079:web:9895ade715d45f0a11fdba'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
